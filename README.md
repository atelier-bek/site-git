# Le git du site.

## INTENTIONS
Rendre Git plus visuel.

## TO DO LIST
~~* Syncroniser automatiquement les répertoires gitlab et repertoires sur notre site (voir api gitlab ?)~~
    * problèmes de droit de groupe sur le server -> group www-data:www-data
  * Arboréscence des dossiers et fichiers
    * Miniature et picto par types de fichiers
    * Description des dossiers
      * Protocole de redaction dans le Readme
  * Functionnalité git
    * visualisation + tri par Commit/Log + Branch.
    * clone + dl zip.
    * Download chaque fichier.
    * Dif fichiers.
  * Visualisation de fichiers
    * Template par type de fichier (images, typo, code, texte, pdf, son, vidéo,
    fichier non supporté)
    * Layout multi-visualisation/cohabitation de différents fichiers.
    * voir le poid des fichiers
  * Photo (une mais bien choisi) de petit chat (noir, mais avec un sourire)
