<?php

include('login.php');

$name = $_GET['name'];
$repo = $_GET['repo'];
$pwd = shell_exec('pwd ');
$pwd = preg_replace('/\s+/', '', $pwd);
$dir = $pwd.'/repo';
$dirRepo = $dir.'/'.$repo;


	$commands = array(
        'cd '.$dirRepo.' && git pull https://'.$user.':'.$pass.'@gitlab.com/'.$name.'/'.$repo.'.git master', 
        'cd '.$dir.' && git clone https://'.$user.':'.$pass.'@gitlab.com/'.$name.'/'.$repo.'.git',
        "git log --graph --abbrev-commit --decorate --date=relative --format=format:'%C(bold blue)%h%C - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all"

      );
    $output = '';

      if (is_dir($dirRepo)){
        $action = 'git pull';
		$tmp = shell_exec($commands[0]);
		shell_exec($commands[0]);
		$output .= "<span style=\"color: #6BE234;\">\$</span> <span style=\"color: #729FCF;\">{$action}\n</span>";
		$output .= htmlentities(trim($tmp)) . "\n";
      }else{

        $action = 'git clone';
		$tmp = shell_exec($commands[1]);
		shell_exec($commands[1]);
		$output .= "<span style=\"color: #6BE234;\">\$</span> <span style=\"color: #729FCF;\">{$commands[1]}\n</span>";
		$output .= htmlentities(trim($tmp)) . "\n";
      }
?>

<!DOCTYPE HTML>
<html lang="fr-Fr">
<head>
	<meta charset="UTF-8">
	<title>GIT DEPLOYMENT SCRIPT</title>
</head>
<body style="background-color: darkblue; color: #FFFFFF; font-weight: bold; padding: 0 10px;">
  <pre>
    <?php 
      echo '<u>git account :</u> '.$name.'<br>';
      echo '<u>name repository :</u> '.$repo.'<br>';
      echo '<u>directory server :</u> '.$dirRepo.'<br>'; 
      echo '<u>action :</u> '.$action.'<br><br>';
      echo $output;
      echo shell_exec($commands[2]);
    ?>
  </pre>
</body>
</html>
