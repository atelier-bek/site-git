<?php
  include('functions/functions.php');
?>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="css/reset-style.css" />
    <link rel="stylesheet" type="text/css" href="js/styles/railscasts.css" />
    <link rel="stylesheet/less" type="text/css" href="css/style.less" />
    <script type="text/javascript" src="js/less.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="js/jquery-3.1.1.min.js" charset="utf-8"></script>
    <!-- <script type="text/javascript" src="js/highlight.pack.js" charset="utf&#45;8"></script> -->
    <script type="text/javascript" src="js/main.js" charset="utf-8"></script>

    <title>site-git</title>    
  </head>
  <body>

