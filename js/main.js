var increm = 0;

var tableFormat = [];
    // FONT
    tableFormat['ttf']   = ['TrueType','font'];
    tableFormat['otf']   = ['OpenType', 'font'];
    tableFormat['sfd']   = ['OpenType', 'font'];
    //CODE
    tableFormat['php']   = ['PHP', 'code'];
    tableFormat['html']  = ['HTML', 'code'];
    tableFormat['css']   = ['CSS', 'code'];
    tableFormat['less']  = ['LESS', 'code'];
    tableFormat['js']    = ['JavaScript', 'code'];
    tableFormat['sh']    = ['Shell', 'code'];
    //TEXT
    tableFormat['txt']   = ['Text','text'];
    tableFormat['md']    = ['Markdown', 'text'];
    //DOWNLOAD
    tableFormat['zip']   = ['ZIP', 'download'];
    tableFormat['rar']   = ['RAR', 'download'];
    //IMAGES
    tableFormat['jpg']   = ['JPG', 'image'];
    tableFormat['png']   = ['PNG', 'image'];
    tableFormat['gif']   = ['GIF', 'image'];
    tableFormat['svg']   = ['SVG', 'image'];
    //PDF
    tableFormat['pdf']   = ['PortableDocumentFormat', 'pdf'];
    tableFormat['null'] = ['non', 'non'];


document.addEventListener("DOMContentLoaded", function(event) {
  var files = $('.list li');
  var folders = $('.list ul');
  var test = 'test.png';
  files.each(function() {
    var extention = $(this).text();
    var src = $(this).attr('data-src');
    if(extention.indexOf('.') != -1){
      extention = extention.substring(extention.lastIndexOf(".") + 1, extention.length);
    }else{
      extention = 'null';
    }

    $(this).addClass(extention);
    // if(extention !== null){
    //   if(tableFormat[extention][1] == 'image'){
    //     $(this).append('<span> |img</span>');
    //   }else if (tableFormat[extention][1] == 'font') {
    //     $(this).append('<span> |font</span>');
    //   }else{
    //     $(this).append('<span> |autre</span>');
    //   }
    // }
    $(this).attr('data-format', extention);
  });

  function toggleFolder(){
    $('.list ul > ul > li, .list ul > ul > ul').hide();
    $('ul').mouseover(function(){
      $(this).children('li').stop().show('fast');
    })
    $('ul > ul').mouseout(function(){
      $(this).children('li').stop().hide('slow');
    })
  }

  function closeFile(){
    $('.file-close').click(function(){
      $(this).parent().parent().remove();
    })
  }
  
  function buildBlock(build_increm, build_type, build_src, build_name, build_format){
    //Block
      $('<div/>', {
            id: 'block-'+build_increm,
            class: 'block block-'+build_type,
      }).prependTo('.content');
    //Block > TitleBar
    $('<div/>', {
            id: 'file-title-'+build_increm,
            class: 'file-title-'+build_type,
            text: build_name,
      }).prependTo('#block-'+build_increm);

    //Block > TitleBar > close
    $('<a/>', {
            id: 'file-close-'+build_increm,
            class: 'file-close file-close-'+build_type,
            text: ' X',
      }).appendTo('#block-'+build_increm+' .file-title-'+build_type);

    //Block > file-content
      $('<div/>', {
          id: 'file-content-'+build_type,
          class: 'file-content',
      }).appendTo('#block-'+build_increm);

      $('.content #block-'+build_increm+' .file-content').load('templates/types/'+build_type+'.php?source='+build_src+'&format='+build_format);
    closeFile();
  }
  
  function loadTemp(){

    $('li').click(function(){
      var src = $(this).attr('data-src');
      var typeTab = $(this).attr('class');
      var type = tableFormat[typeTab][1];
      var format = tableFormat[typeTab][0];
      var name = $(this).text();
      buildBlock(increm, type, src, name, format);
      increm++;
    })
  }
  loadTemp();
  toggleFolder();

});
